<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ 'LegalAct' }}</title>

    <!-- Scripts -->
    <script src="{{ asset('assets/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">

        <nav class="nav">
            <div style="padding-left: 1rem;">
                <a href="/dashboard" class="nav-back">
                    <img src="/assets/img/icons/Arrow-Left.png" style="width: auto;max-width: 110px;" alt="Main Page">
                </a>
            </div>
            <div class="nav-elem">
                @guest
                @else
                <div class="nav-user">
                    <img src="/assets/img/icons/Work.png" alt="Work">
                    <p>{{ Auth::user()->name }}</p>
                </div>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="nav-exit">
                    <img src="/assets/img/icons/Logout.png" alt="Logout">
                </a>
                @endguest
            </div>
        </nav>

        <main style="padding-top:1rem;">
            @yield('content')
        </main>
    </div>
</body>

</html>