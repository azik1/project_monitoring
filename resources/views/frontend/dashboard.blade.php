@extends('layouts.app')

@section('content')
<!-- home start -->
<div class="home">
    <div class="container">
        <div class="home-top">
            <div class="home-top__left">
                <img src="/assets/img/icons/Show.png" alt="Show">
                <p>{{ __('lang.header_slogan')}}</p>
            </div>
            <div class="home-top__right">
                <div>
                    <span> <a href="/en/dashboard">EN</a></span> /
                    <span> <a href="/ru/dashboard">RU</a></spanvalue=>
                </div>
            </div>
        </div>
    </div>

    <div class="container home-bill">
        <div class="home-card">
            <div class="home-card__top">{{ __('lang.calc')}}</div>
            <div class="home-card__body">
                <div class="home-card__items">
                    <p>{{ __('lang.all_sum')}}</p>
                    <span>{{$total_sum}} {{ __('lang.sum')}}</span>
                </div>
                <div class="home-card__items">
                    <p>{{ __('lang.all_sum_peni')}}</p>
                    <span>{{$total_peni}} {{ __('lang.sum')}}</span>
                </div>
                <div class="home-card__total">
                    <p>{{ __('lang.total')}}</p>
                    <span>{{$total_sum_peni}} {{ __('lang.sum')}}</span>
                </div>
            </div>
        </div>
        <div class="home-card">
            <div class="home-card__top">{{ __('lang.priznano_sudom')}}</div>
            <div class="home-card__body">
                <div class="home-card__items">
                    <p>{{ __('lang.all_sum')}}</p>
                    <span>{{$total_priznano_sum}} {{ __('lang.sum')}}</span>
                </div>
                <div class="home-card__items">
                    <p>{{ __('lang.all_sum_peni')}}</p>
                    <span>{{$total_priznano_peni}} {{ __('lang.sum')}}</span>
                </div>
                <div class="home-card__total">
                    <p>{{ __('lang.total')}}</p>
                    <span>{{$total_priznano_sum_peni}} {{ __('lang.sum')}}</span>
                </div>
            </div>
        </div>
        <div class="home-card">
            <div class="home-card__top home-card__top_red">{{ __('lang.collected')}}</div>
            <div class="home-card__body">
                <div class="home-card__items">
                    <p>{{ __('lang.all_sum')}}</p>
                    <span>{{$total_vzikano}} {{ __('lang.sum')}}</span>
                </div>
                <div class="home-card__items">
                    <p>{{ __('lang.all_sum_peni')}}</p>
                    <span>{{$total_vzikano_peni}} {{ __('lang.sum')}}</span>
                </div>
                <div class="home-card__total">
                    <p>{{ __('lang.total')}}</p>
                    <span>{{$total_vzikano_sum_peni}} {{ __('lang.sum')}}</span>
                </div>
            </div>
        </div>
        <div class="home-card home-card_procent">
            <div class="home-card__top home-card__top_red">{{ __('lang.in_percentages')}}</div>
            <div class="home-card__body">
                <div class="home-card__items">
                    <p>{{ __('lang.debt') }}</p>
                    <span>{{$total_all_dolg_prosent}}%</span>
                </div>
                <div class="home-card__items">
                    <p>{{ __('lang.peni') }}</p>
                    <span>{{$total_all_peni_prosent}}%</span>
                </div>
                <div class="home-card__total">
                    <p>{{ __('lang.total') }}</p>
                    <span>{{$total_all_prosent}}%</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- home end -->

<!-- content list -->
<div class="container">
    <!-- <div class="home-search">
        <label for="search">Поиск</label>
        <div class="home-search__inner">
            <input type="text" id="search" placeholder="Поиск">
            <img src="/assets/img/icons/Search.png" alt="Search">
        </div>
    </div> -->
    <ul class="home-buttons home-buttons__left nav nav-tabs" id="myTab" role="tablist">
        <li class="home-buttons__inner nav-item" role="presentation">
            <button class="home-buttons__btn nav-link active" id="all-tab" data-bs-toggle="tab" data-bs-target="#all" type="button" role="tab" aria-controls="all" aria-selected="true">{{ __('lang.debtors')}}</button>
            @foreach($debtors as $index=>$item)
            <span>{{$item->count();}}</span>
            @endforeach
        </li>

        <li class="home-buttons__inner nav-item" role="presentation">
            <button class="home-buttons__btn nav-link" id="first-tab" data-bs-toggle="tab" data-bs-target="#first" type="button" role="tab" aria-controls="first" aria-selected="false">1-{{ __('lang.stage')}}</button>
            <span>{{$stage_first->count()}}</span>
        </li>

        <li class="home-buttons__inner nav-item" role="presentation">
            <button class="home-buttons__btn nav-link" id="pills-second-tab" data-bs-toggle="tab" data-bs-target="#second" type="button" role="tab" aria-controls="second" aria-selected="false">2-{{ __('lang.stage')}}</button>
            <span>{{$stage_second->count()}}</span>
        </li>

        <li class="home-buttons__inner nav-item" role="presentation">
            <button class="home-buttons__btn nav-link" id="pills-three-tab" data-bs-toggle="tab" data-bs-target="#three" type="button" role="tab" aria-controls="three" aria-selected="false">3-{{ __('lang.stage')}}</button>
            <span>{{$stage_three->count()}}</span>
        </li>

        <li class="home-buttons__inner nav-item" role="presentation">
            <button class="home-buttons__btn nav-link red" id="pills-end-tab" data-bs-toggle="tab" data-bs-target="#end" type="button" role="tab" aria-controls="end" aria-selected="false">{{ __('lang.complete')}}</button>
            <span>{{$stage_end->count()}}</span>
        </li>

    </ul>
    <!-- <div class=""> -->
    <div class="home-table__wrapper" id="myTabContent">
        <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
            <table class="tab-content home-table">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>{{ __('lang.naimenovaniye_doljnika')}}</th>
                        <th>1-{{ __('lang.stage')}}</th>
                        <th>{{ __('lang.raschet_summa_iska')}}</th>
                        <th>2-{{ __('lang.stage')}}</th>
                        <th>3-{{ __('lang.stage')}}</th>
                        <th>{{ __('lang.prizanannaya_summa_sudom')}}</th>
                        <th>{{ __('lang.summa_vziskannoy_zadojennosti')}}</th>
                        <th>{{ __('lang.prosent_vziskannoy_zadoljennosti')}}</th>
                        <th>{{ __('lang.vziskaniye_drugix_sudebnix_rasxodov')}}</th>
                    </tr>
                </thead>

                @php $i = 1; @endphp
                @foreach($debtors as $index=>$item)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>
                        <p class="home-table__name">{{ $item->name }}</p>
                    </td>
                    <td>
                        @if ($item->stages <= 2) <p class="home-table__etap">1-{{ __('lang.stage')}}</p>
                            <button class="home-table__btn home-table__btn_pink"><a href="/dashboard/{{$item->slug}}">{{ __('lang.in_progress')}}</a></button>
                            @else
                            <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                            @endif
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_dolgi }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_dolgi_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        @if ($item->stages >= 3 && $item->stages < 6) <p class="home-table__etap">2-{{ __('lang.stage')}}</p>
                            <button class="home-table__btn home-table__btn_pink"><a href="/dashboard/{{$item->slug}}">{{ __('lang.in_progress')}}</a></button>
                            @elseif ($item->stages >= 5)
                            <p class="home-table__etap">2-{{ __('lang.stage')}}</p>
                            <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                            @else
                            @endif
                    </td>
                    <td>
                        @if ($item->stages >= 6)
                        <p class="home-table__etap">3-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn home-table__btn_pink"><a href="/dashboard/{{$item->slug}}">{{ __('lang.in_progress')}}</a></button>
                        @elseif ($item->stages >= 13)
                        <p class="home-table__etap">3-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                        @else
                        @endif
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_priznano }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_priznano_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano / $item->summa_dolgi *100 }}%</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano_peni / $item->summa_dolgi_peni *100 }}%</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_other }} {{ __('lang.sum')}}</p>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        <div class="tab-pane fade" id="first" role="tabpanel" aria-labelledby="first-tab">
            @if(!$stage_first->count())
            <p class="text-muted" style="margin: 0;padding-top: 3rem;text-align: center;">Нет данных</p>
            @else
            <table class="tab-content home-table">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>{{ __('lang.name_debtor') }}</th>
                        <th>1-{{ __('lang.stage') }}</th>
                        <th>{{ __('lang.calc_amount') }}</th>
                        <th>2-{{ __('lang.stage') }}</th>
                        <th>3-{{ __('lang.stage') }}</th>
                        <th>{{ __('lang.prizanannaya_summa_sudom')}}</th>
                        <th>{{ __('lang.summa_vziskannoy_zadojennosti')}}</th>
                        <th>{{ __('lang.prosent_vziskannoy_zadoljennosti')}}</th>
                        <th>{{ __('lang.vziskaniye_drugix_sudebnix_rasxodov')}}</th>
                    </tr>
                </thead>

                @foreach($stage_first as $index=>$item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>
                        <p class="home-table__name">{{ $item->name }}</p>
                    </td>
                    <td>
                        @if ($item->stages <= 2) <p class="home-table__etap">1-{{ __('lang.stage')}}</p>
                            <button class="home-table__btn home-table__btn_pink"><a href="/dashboard/{{$item->slug}}">{{ __('lang.in_progress')}}</a></button>
                            @else
                            <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                            @endif
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_dolgi }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_dolgi_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        @if ($item->stages == 3)
                        <p class="home-table__etap">2-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn home-table__btn_pink"><a href="/dashboard/{{$item->slug}}">{{ __('lang.in_progress')}}</a></button>
                        @elseif ($item->stages >= 4)
                        <p class="home-table__etap">2-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                        @else
                        @endif
                    </td>
                    <td>
                        @if ($item->stages >= 5)
                        <p class="home-table__etap">3-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn home-table__btn_pink"><a href="/dashboard/{{$item->slug}}">{{ __('lang.in_progress')}}</a></button>
                        @elseif ($item->stages >= 12)
                        <p class="home-table__etap">3-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                        @else
                        @endif
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_priznano }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_priznano_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano / $item->summa_dolgi *100 }}%</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano_peni / $item->summa_dolgi_peni *100 }}%</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_other }} {{ __('lang.sum')}}</p>
                    </td>
                </tr>
                @endforeach
            </table>
            @endif

        </div>
        <div class="tab-pane fade" id="second" role="tabpanel" aria-labelledby="second-tab">
            @if(!$stage_second->count())
            <p class="text-muted" style="margin: 0;padding-top: 3rem;text-align: center;">Нет данных</p>
            @else
            <table class="tab-content home-table">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>{{ __('lang.name_debtor') }}</th>
                        <th>1-{{ __('lang.stage') }}</th>
                        <th>{{ __('lang.calc_amount') }}</th>
                        <th>2-{{ __('lang.stage') }}</th>
                        <th>3-{{ __('lang.stage') }}</th>
                        <th>{{ __('lang.prizanannaya_summa_sudom')}}</th>
                        <th>{{ __('lang.summa_vziskannoy_zadojennosti')}}</th>
                        <th>{{ __('lang.prosent_vziskannoy_zadoljennosti')}}</th>
                        <th>{{ __('lang.vziskaniye_drugix_sudebnix_rasxodov')}}</th>
                    </tr>
                </thead>

                @foreach($stage_second as $index=>$item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>
                        <p class="home-table__name">{{ $item->name }}</p>
                    </td>
                    <td>
                        <p class="home-table__etap">1-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_dolgi }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_dolgi_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        @if ($item->stages === 4)
                        <p class="home-table__etap">2-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn home-table__btn_pink"><a href="/dashboard/{{$item->slug}}">{{ __('lang.in_progress')}}</a></button>
                        @else ($item->stages === 5)
                        <p class="home-table__etap">2-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                        @endif
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_priznano }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_priznano_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano / $item->summa_dolgi *100 }}%</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano_peni / $item->summa_dolgi_peni *100 }}%</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_other }} {{ __('lang.sum')}}</p>
                    </td>
                </tr>
                @endforeach
            </table>
            @endif

        </div>
        <div class="tab-pane fade" id="three" role="tabpanel" aria-labelledby="three-tab">
            @if(!$stage_three->count())
            <p class="text-muted" style="margin: 0;padding-top: 3rem;text-align: center;">Нет данных</p>
            @else
            <table class="tab-content home-table">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>{{ __('lang.naimenovaniye_doljnika')}}</th>
                        <th>1-{{ __('lang.stage')}}</th>
                        <th>{{ __('lang.raschet_summa_iska')}}</th>
                        <th>2-{{ __('lang.stage')}}</th>
                        <th>3-{{ __('lang.stage')}}</th>
                        <th>{{ __('lang.prizanannaya_summa_sudom')}}</th>
                        <th>{{ __('lang.summa_vziskannoy_zadojennosti')}}</th>
                        <th>{{ __('lang.prosent_vziskannoy_zadoljennosti')}}</th>
                        <th>{{ __('lang.vziskaniye_drugix_sudebnix_rasxodov')}}</th>
                    </tr>
                </thead>

                @foreach($stage_three as $index=>$item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>
                        <p class="home-table__name">{{ $item->name }}</p>
                    </td>
                    <td>
                        <p class="home-table__etap">1-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_dolgi }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_dolgi_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__etap">2-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                    </td>
                    <td>
                        @if ($item->stages >= 5)
                        <p class="home-table__etap">3-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn home-table__btn_pink">{{ __('lang.in_progress')}}</a></button>
                        @elseif ($item->stages >= 12)
                        <p class="home-table__etap">3-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn">{{ __('lang.completed')}}</a></button>
                        @else
                        @endif
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_priznano }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_priznano_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano / $item->summa_dolgi *100 }}%</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano_peni / $item->summa_dolgi_peni *100 }}%</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_other }} {{ __('lang.sum')}}</p>
                    </td>
                </tr>
                @endforeach
            </table>
            @endif
        </div>
        <div class="tab-pane fade" id="end" role="tabpanel" aria-labelledby="end-tab">
            @if(!$stage_end->count())
            <p class="text-muted" style="margin: 0;padding-top: 3rem;text-align: center;">Нет данных</p>
            @else
            <table class="tab-content home-table">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>{{ __('lang.name_debtor') }}</th>
                        <th>1-{{ __('lang.stage') }}</th>
                        <th>{{ __('lang.calc_amount') }}</th>
                        <th>2-{{ __('lang.stage') }}</th>
                        <th>3-{{ __('lang.stage') }}</th>
                        <th>{{ __('lang.prizanannaya_summa_sudom')}}</th>
                        <th>{{ __('lang.summa_vziskannoy_zadojennosti')}}</th>
                        <th>{{ __('lang.prosent_vziskannoy_zadoljennosti')}}</th>
                        <th>{{ __('lang.vziskaniye_drugix_sudebnix_rasxodov')}}</th>
                    </tr>
                </thead>

                @foreach($stage_end as $index=>$item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>
                        <p class="home-table__name">{{ $item->name }}</p>
                    </td>
                    <td>
                        <p class="home-table__etap">1-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn">{{ __('lang.completed')}}</button>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_dolgi }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_dolgi_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__etap">2-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                    </td>
                    <td>
                        @if ($item->stages >= 5)
                        <p class="home-table__etap">3-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn home-table__btn_pink"><a href="/dashboard/{{$item->slug}}">{{ __('lang.in_progress')}}</a></button>
                        @elseif ($item->stages >= 12)
                        <p class="home-table__etap">3-{{ __('lang.stage')}}</p>
                        <button class="home-table__btn"><a href="/dashboard/{{$item->slug}}">{{ __('lang.completed')}}</a></button>
                        @else
                        @endif
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_priznano }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_priznano_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano }} {{ __('lang.sum')}}</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano_peni }} {{ __('lang.sum')}}</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano / $item->summa_dolgi *100 }}%</p>
                        <p class="home-table__title">{{ __('lang.po_peni')}}</p>
                        <p class="home-table__sum">{{ $item->summa_vziskano_peni / $item->summa_dolgi_peni *100 }}%</p>
                    </td>
                    <td>
                        <p class="home-table__title">{{ __('lang.po_osnovnomu_dolgu')}}</p>
                        <p class="home-table__sum">{{ $item->summa_other }} {{ __('lang.sum')}}</p>
                    </td>
                </tr>
                @endforeach
            </table>
            @endif

        </div>
    </div>
    <!-- </div> -->
</div>
<!-- content end -->

@endsection