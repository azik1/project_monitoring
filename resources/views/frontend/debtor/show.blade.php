@extends('layouts.app')

@section('content')

<div class="stages">
    <div class="stages-title">{{$data->name}}</div>
    <div class="stages-blog">
        <div class="stages-blog__item @if($data->stages == 1)stages-red @elseif ($data->stages > 1) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                @if($data->stages == 1)<img src="/assets/img/icons/red-tick.png" alt="red-tick"> @elseif ($data->stages > 1) <img src="/assets/img/icons/green-tick.png" alt="red-tick"> @else <img src="/assets/img/icons/grey-tick.png" alt="red-tick"> @endif
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">1-{{ __('lang.stage')}}. {{ __('lang.izucheniye')}}</div>
                <div class="stages-blog__content_text">{{ __('lang.prosess_naxoditsya_izujeniye')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 2)stages-red @elseif ($data->stages > 2) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                @if($data->stages == 2)<img src="/assets/img/icons/red-tick.png" alt="red-tick"> @elseif ($data->stages > 2) <img src="/assets/img/icons/green-tick.png" alt="red-tick"> @else <img src="/assets/img/icons/grey-tick.png" alt="red-tick"> @endif
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">1-{{ __('lang.stage')}}. {{ __('lang.raschot_summi_iska')}}</div>
                <div class="stages-blog__content_text"><span>{{ __('lang.po_osnovnomu_dolgu')}}:</span>100 {{ __('lang.sum')}} <span>{{ __('lang.po_peni')}}:</span> 10 {{ __('lang.sum')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 3)stages-red @elseif ($data->stages > 3) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                @if($data->stages == 3)<img src="/assets/img/icons/red-tick.png" alt="red-tick"> @elseif ($data->stages > 3) <img src="/assets/img/icons/green-tick.png" alt="red-tick"> @else <img src="/assets/img/icons/grey-tick.png" alt="red-tick"> @endif
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">2-{{ __('lang.stage')}}. {{ __('lang.do_sudebnogo_resheniye')}}</div>
                <div class="stages-blog__content_text">{{ __('lang.podgotovka_trebovaniya')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 4)stages-red @elseif ($data->stages > 4) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                @if($data->stages === 4)<img src="/assets/img/icons/red-tick.png" alt="red-tick"> @elseif ($data->stages > 4) <img src="/assets/img/icons/green-tick.png" alt="red-tick"> @else <img src="/assets/img/icons/grey-tick.png" alt="red-tick"> @endif
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">2-{{ __('lang.stage')}}. {{ __('lang.do_sudebnogo_resheniye')}}</div>
                <div class="stages-blog__content_text">{{ __('lang.otpravka_trebovaniya')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 5)stages-red @elseif ($data->stages > 5) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                @if($data->stages === 5)<img src="/assets/img/icons/red-tick.png" alt="red-tick"> @elseif ($data->stages > 5) <img src="/assets/img/icons/green-tick.png" alt="green-green"> @else <img src="/assets/img/icons/grey-tick.png" alt="grey-tick"> @endif
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">2-{{ __('lang.stage')}}. {{ __('lang.do_sudebnogo_resheniye')}}</div>
                <div class="stages-blog__content_text">{{ __('lang.peregovoty_s_doljnikom')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 6)stages-red @elseif ($data->stages > 6) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                @if($data->stages === 6)<img src="/assets/img/icons/red-tick.png" alt="red-tick"> @elseif ($data->stages > 6) <img src="/assets/img/icons/green-tick.png" alt="green-green"> @else <img src="/assets/img/icons/grey-tick.png" alt="grey-tick"> @endif
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-{{ __('lang.stage')}}. {{ __('lang.resheniye_spora_sudebnim')}}</div>
                <div class="stages-blog__content_text">{{ __('lang.podgotovka_iska')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 7)stages-red @elseif ($data->stages > 7) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                @if($data->stages === 7)<img src="/assets/img/icons/red-tick.png" alt="red-tick"> @elseif ($data->stages > 7) <img src="/assets/img/icons/green-tick.png" alt="green-green"> @else <img src="/assets/img/icons/grey-tick.png" alt="grey-tick"> @endif
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-{{ __('lang.stage')}}. {{ __('lang.resheniye_spora_sudebnim')}}</div>
                <div class="stages-blog__content_text">{{ __('lang.otpravka_iska')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 8)stages-red @elseif ($data->stages > 8) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                @if($data->stages === 8)<img src="/assets/img/icons/red-tick.png" alt="red-tick"> @elseif ($data->stages > 8) <img src="/assets/img/icons/green-tick.png" alt="green-green"> @else <img src="/assets/img/icons/grey-tick.png" alt="grey-tick"> @endif
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-{{ __('lang.stage')}}. {{ __('lang.resheniye_spora_sudebnim')}}</div>
                <div class="stages-blog__content_text">{{ __('lang.rassmotreniye_dela_v_sude')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 9)stages-red @elseif ($data->stages > 9) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                @if($data->stages === 9)<img src="/assets/img/icons/red-tick.png" alt="red-tick"> @elseif ($data->stages > 9) <img src="/assets/img/icons/green-tick.png" alt="green-green"> @else <img src="/assets/img/icons/grey-tick.png" alt="grey-tick"> @endif
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-{{ __('lang.stage')}}. {{ __('lang.resheniye_spora_sudebnim')}}</div>
                <div class="stages-blog__content_text">{{ __('lang.poluchenie_resheniye')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 10)stages-red @elseif ($data->stages > 10) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                <img src="/assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-{{ __('lang.stage')}}. {{ __('lang.resheniye_spora_sudebnim')}} / {{ __('lang.priznannaya_sudom')}}</div>
                <div class="stages-blog__content_text"><span>{{ __('lang.po_osnovnomu_dolgu')}}:</span> 0 {{ __('lang.sum')}} <span>{{ __('lang.po_peni')}}:</span> 0 {{ __('lang.sum')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 11)stages-red @elseif ($data->stages > 11) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                <img src="/assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-{{ __('lang.stage')}}. {{ __('lang.resheniye_spora_sudebnim')}}</div>
                <div class="stages-blog__content_text">{{ __('lang.otpravka_dokumentov')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 12)stages-red @elseif ($data->stages > 12) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                <img src="/assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-{{ __('lang.stage')}}. {{ __('lang.resheniye_spora_sudebnim')}}</div>
                <div class="stages-blog__content_text">{{ __('lang.ispolneniya_resheniya')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 13)stages-red @elseif ($data->stages > 13) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                <img src="/assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">{{ __('lang.amount_debt')}}</div>
                <div class="stages-blog__content_text"><span>{{ __('lang.po_osnovnomu_dolgu')}}:</span> 0 {{ __('lang.sum')}} <span>{{ __('lang.po_peni')}}:</span> 0 {{ __('lang.sum')}}</div>
            </div>
        </div>
        <div class="stages-blog__item @if($data->stages == 13)stages-red @elseif ($data->stages > 13) stages-green @else stages-grey @endif">
            <div class="stages-blog__arrow">
                <img src="/assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_end">{{ __('lang.completed')}}</div>
            </div>
        </div>
    </div>
</div>

@endsection