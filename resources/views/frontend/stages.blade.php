@extends('layouts.app')

@section('content')
<div class="stages">
    <div class="stages-title">OOO “COMPANY NAME”</div>
    <div class="stages-blog">
        <div class="stages-blog__item stages-green">
            <div class="stages-blog__arrow">
                <img src="/assets/img/icons/green-tick.png" alt="green-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">1-этап. Изучение</div>
                <div class="stages-blog__content_text">Процесс находиться в стадии изучение</div>
            </div>
        </div>
        <div class="stages-blog__item stages-green">
            <div class="stages-blog__arrow">
                <img src="/assets/img/icons/green-tick.png" alt="green-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">1-этап. Расчет суммы иска</div>
                <div class="stages-blog__content_text"><span>По основному долгу:</span>100 сум <span>По пени:</span> 10 сум</div>
            </div>
        </div>
        <div class="stages-blog__item stages-red">
            <div class="stages-blog__arrow">
                <img src="/assets/img/icons/red-tick.png" alt="red-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">2-этап. До судебное решение спора</div>
                <div class="stages-blog__content_text">Подготовка требования</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="/assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">2-этап. До судебное решение спора</div>
                <div class="stages-blog__content_text">Отправка требования</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="../assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">2-этап. До судебное решение спора</div>
                <div class="stages-blog__content_text">Переговоры с должником</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="../assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-этап. Решение спора судебным разбирательством</div>
                <div class="stages-blog__content_text">Подготовка иска</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="../assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-этап. Решение спора судебным разбирательством</div>
                <div class="stages-blog__content_text">Отправка иска</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="../assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-этап. Решение спора судебным разбирательством</div>
                <div class="stages-blog__content_text">Рассмотрение дела в суде</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="../assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-этап. Решение спора судебным разбирательством</div>
                <div class="stages-blog__content_text">Получение решение и исполнительного листа</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="../assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-этап. Решение спора судебным разбирательством / Признанная сумма судом</div>
                <div class="stages-blog__content_text"><span>По основному долгу:</span> 0 сум <span>По пени:</span> 0 сум</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="../assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-этап. Решение спора судебным разбирательством</div>
                <div class="stages-blog__content_text">Отправка документов в БПИ</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="../assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">3-этап. Решение спора судебным разбирательством</div>
                <div class="stages-blog__content_text">Исполнения решения суда</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="../assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_title">Сумма взысканной задолженности</div>
                <div class="stages-blog__content_text"><span>По основному долгу:</span> 0 сум <span>По пени:</span> 0 сум</div>
            </div>
        </div>
        <div class="stages-blog__item stages-grey">
            <div class="stages-blog__arrow">
                <img src="../assets/img/icons/grey-tick.png" alt="grey-tick">
            </div>
            <div class="stages-blog__content">
                <div class="stages-blog__content_end">ЗАВРЕШЕНО</div>
            </div>
        </div>
    </div>
</div>

@endsection