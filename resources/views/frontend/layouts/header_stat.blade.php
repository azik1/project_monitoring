@extends('layouts.app')

@section('content')
<!-- home start -->
<div class="home">
    <div class="container">
        <div class="home-top">
            <div class="home-top__left">
                <img src="/assets/img/icons/Show.png" alt="Show">
                <p>Мониторинг процесс взыскания дебиторской задолженности</p>
            </div>
            <div class="home-top__right">
                <img src="/assets/img/icons/Arrow-Down.png" alt="Arrow-Down">
                <select>
                    <option value="RU">RU</option>
                    <option value="EN">EN</option>
                </select>
            </div>
        </div>
    </div>

    <div class="home-bill">
        <div class="home-card">
            <div class="home-card__top">Рассчитано</div>
            <div class="home-card__body">
                <div class="home-card__items">
                    <p>Общая сумма основного долга</p>
                    <span>{{$total_sum}} сум</span>
                </div>
                <div class="home-card__items">
                    <p>Общая сумма пении</p>
                    <span>{{$total_peni}} сум</span>
                </div>
                <div class="home-card__total">
                    <p>Всего</p>
                    <span>{{$total_sum_peni}} сум</span>
                </div>
            </div>
        </div>
        <div class="home-card">
            <div class="home-card__top">Признано судом</div>
            <div class="home-card__body">
                <div class="home-card__items">
                    <p>Общая сумма основного долга</p>
                    <span>{{$total_priznano_sum}} сум</span>
                </div>
                <div class="home-card__items">
                    <p>Общая сумма пении</p>
                    <span>{{$total_priznano_peni}} сум</span>
                </div>
                <div class="home-card__total">
                    <p>Всего</p>
                    <span>{{$total_priznano_sum_peni}} сум</span>
                </div>
            </div>
        </div>
        <div class="home-card">
            <div class="home-card__top home-card__top_red">Взыскано</div>
            <div class="home-card__body">
                <div class="home-card__items">
                    <p>Общая сумма основного долга</p>
                    <span>{{$total_vzikano}} сум</span>
                </div>
                <div class="home-card__items">
                    <p>Общая сумма пении</p>
                    <span>{{$total_vzikano_peni}} сум</span>
                </div>
                <div class="home-card__total">
                    <p>Всего</p>
                    <span>{{$total_vzikano_sum_peni}} сум</span>
                </div>
            </div>
        </div>
        <div class="home-card home-card_procent">
            <div class="home-card__top home-card__top_red">В процентах</div>
            <div class="home-card__body">
                <div class="home-card__items">
                    <p>Долга</p>
                    <span>{{$total_all_dolg_prosent}}%</span>
                </div>
                <div class="home-card__items">
                    <p>Пени</p>
                    <span>{{$total_all_peni_prosent}}%</span>
                </div>
                <div class="home-card__total">
                    <p>Всего</p>
                    <span>{{$total_all_prosent}}%</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- home end -->

<!-- content list -->
<!-- content end -->

@endsection