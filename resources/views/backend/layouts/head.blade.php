<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ 'LegalAct' }}</title>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
</head>

<body>
    <div id="app">

        <div class="wrapper d-flex align-items-stretch">
            <nav id="sidebar">
                <div class="p-4">
                    <a href="/admin/debtor" class="mb-5">
                        <img src="/assets/img/sidebar-logo.png" alt="" class="h-40 mb-4">
                    </a>
                    <ul class="list-unstyled components mb-5">
                        <li class="active">
                            <a href="/admin/debtor">Должники</a>
                        </li>
                        <li>
                            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Система</a>
                            <ul class="collapse list-unstyled" id="homeSubmenu">
                                <li>
                                    <a href="#">Пользователи</a>
                                </li>
                                <li>
                                    <a href="#">Доступы</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <div class="footer">
                        <p>
                            Copyright &copy;<script>
                                document.write(new Date().getFullYear());
                            </script> by <a href="https://legalact.uz" target="_blank">LegalAct</a>
                        </p>
                    </div>
                </div>
            </nav>

            <div id="content" class="container p-4">

                @yield('content')
                <br>
            </div>

        </div>

    </div>

    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/popper.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/main.js"></script>
    <script defer src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194" integrity="sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw==" data-cf-beacon='{"rayId":"6df53c60ac78376d","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2021.12.0","si":100}' crossorigin="anonymous"></script>

</body>


</html>