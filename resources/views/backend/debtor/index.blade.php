@extends('backend.layouts.head')

@section('content')


<nav class="navbar navbar-expand-lg navbar-light bg-light">
    @include('backend.layouts.menu_item')
    <ul class="nav navbar-nav ml-auto">
        <a href="debtor/create">
            <li class="btn btn-secondary " style="float: right;"> Добавить </li>
        </a>
    </ul>
</nav>

<div class="mt-5">
    <h1>{{ __('lang.all_debtors') }}</h1>
</div>
<div class="mb-2">
    <input class="form-control" type="text" id="myInput" onkeyup="myFunction()" placeholder="Названия">
</div>

<table class="table table-striped" id="myTable">
    <thead>
        <tr class="bg-warning">
            <th scope="col">#</th>
            <th scope="col">{{ __('lang.naimenovaniye_doljnika')}}</th>
            <th scope="col" style="text-align: center;">{{ __('lang.all_sum')}}</th>
            <th scope="col" style="text-align: center;">Действия</th>
        </tr>
    </thead>
    <tbody>
        @php $i = 1; @endphp
        @foreach ($data as $item)
        <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{ $item->name }}</td>
            <td style="text-align: center;">{{ $item->summa_dolgi }}</td>
            <td style="text-align: center;">
                <a href="{{ route('debtor.edit',$item->id) }}">
                    <btn class="btn-sm btn-secondary">Изменить</btn>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    function myFunction() {
        // Declare variables
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>

@endsection