@extends('backend.layouts.head')

@section('content')

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    @include('backend.layouts.menu_item')
</nav>

<div class="mt-5">
    <h1>{{ __('lang.all_debtors') }}</h1>
    <hr>
    <p style="color: #ff0000;">* все поля должны быть заполнены!</p>
</div>


<div class="border p-3 bg-light">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="post" action="{{ route('debtor.store') }}">
        @csrf

        <div class="mb-3">
            <label for="name" class="form-label font-weight-bold">Названия</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Имя должника">
        </div>

        <div class="mb-3">
            <label for="stages" class="form-label font-weight-bold">Процесс</label>
            <select name="stages" id="stages" class="custom-select">>
                <option selected value="0">Выберите процесс</option>
                <option disabled>1-этап</option>
                <option value="1">Изучение</option>
                <option value="2">Расчет суммы иска</option>
                <option disabled>2 этап</option>
                <option value="3">До судебное решение спора / Подготовка требования</option>
                <option value="4">До судебное решение спора / Отправка требования</option>
                <option value="5">До судебное решение спора / Переговоры с должником</option>
                <option disabled>3 этап</option>
                <option value="6">Решение спора судебным разбирательством / Подготовка иска</option>
                <option value="7">Решение спора судебным разбирательством / Отправка иска</option>
                <option value="8">Решение спора судебным разбирательством / Рассмотрение дела в суде</option>
                <option value="9">Решение спора судебным разбирательством / Получение решение и исполнительного листа</option>
                <option value="10">Решение спора судебным разбирательством / Признанная сумма судом</option>
                <option value="11">Решение спора судебным разбирательством / Отправка документов в БПИ</option>
                <option value="12">Решение спора судебным разбирательством / Исполнения решения суда</option>
                <option value="13">Сумма взысканной задолженности</option>
                <option value="14">Завершено</option>
            </select>
        </div>

        <div class="mb-3">
            <label for="summa_dolgi" class="form-label font-weight-bold">Сумма долги</label><br>
            <input type="number" name="summa_dolgi" class="form-control" id="summa_dolgi" placeholder="Введите сумма">
        </div>

        <div class="mb-3">
            <label for="summa_dolgi_peni" class="form-label font-weight-bold">Сумма долги пени</label>
            <input type="number" name="summa_dolgi_peni" class="form-control" id="summa_dolgi_peni" placeholder="Введите сумма">
        </div>

        <button type="submit" class="btn btn-primary">Добавить</button>
        <button type="submit" class="btn btn-secondary" style="float: right;"><a onclick="javascript:history.back(); return false;">Назад</a></button>
    </form>
</div>


@endsection