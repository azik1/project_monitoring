@extends('backend.layouts.head')

@section('content')

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    @include('backend.layouts.menu_item')
</nav>

<div class="mt-5">
    <h1>{{ __('lang.all_debtors') }}</h1>
</div>


<div class="border p-3 bg-light">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="post" action="{{ route('debtor.update',$debtor->id) }}">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="name" class="form-label font-weight-bold">Названия</label>
            <input type="text" name="name" class="form-control" value="{{$debtor->name}}">
        </div>
        <div class="mb-3">
            <label for="stages" class="form-label font-weight-bold">Процесс</label>
            <select name="stages" id="stages" class="custom-select">>
                <option selected value="0">Выберите процесс</option>
                <option disabled>1-этап</option>
                <option value="1" {{ old('stages', $debtor->stages) == 1 ? 'selected' : '' }}>Изучение</option>
                <option value="2" {{ old('stages', $debtor->stages) == 2 ? 'selected' : '' }}>Расчет суммы иска</option>
                <option disabled>2 этап</option>
                <option value="3" {{ old('stages', $debtor->stages) == 3 ? 'selected' : '' }}>До судебное решение спора / Подготовка требования</option>
                <option value="4" {{ old('stages', $debtor->stages) == 4 ? 'selected' : '' }}>До судебное решение спора / Отправка требования</option>
                <option value="5" {{ old('stages', $debtor->stages) == 5 ? 'selected' : '' }}>До судебное решение спора / Переговоры с должником</option>
                <option disabled>3 этап</option>
                <option value="6" {{ old('stages', $debtor->stages) == 6 ? 'selected' : '' }}>Решение спора судебным разбирательством / Подготовка иска</option>
                <option value="7" {{ old('stages', $debtor->stages) == 7 ? 'selected' : '' }}>Решение спора судебным разбирательством / Отправка иска</option>
                <option value="8" {{ old('stages', $debtor->stages) == 8 ? 'selected' : '' }}>Решение спора судебным разбирательством / Рассмотрение дела в суде</option>
                <option value="9" {{ old('stages', $debtor->stages) == 9 ? 'selected' : '' }}>Решение спора судебным разбирательством< / Получение решение и исполнительного листа/option>
                <option value="10" {{ old('stages', $debtor->stages) == 10 ? 'selected' : '' }}>Решение спора судебным разбирательством / Признанная сумма судом</option>
                <option value="11" {{ old('stages', $debtor->stages) == 11 ? 'selected' : '' }}>Решение спора судебным разбирательством / Отправка документов в БПИ</option>
                <option value="12" {{ old('stages', $debtor->stages) == 12 ? 'selected' : '' }}>Решение спора судебным разбирательством / Исполнения решения суда</option>
                <option value="13" {{ old('stages', $debtor->stages) == 13 ? 'selected' : '' }}>Сумма взысканной задолженности</option>
                <option value="14" {{ old('stages', $debtor->stages) == 14 ? 'selected' : '' }}>Завершено</option>
            </select>
        </div>

        <div class="mb-3">
            <label for="summa_dolgi" class="form-label font-weight-bold">Сумма долги</label><br>
            <input type="text" name="summa_dolgi" class="form-control" value="{{$debtor->summa_dolgi}}">
        </div>

        <div class="mb-3">
            <label for="summa_dolgi_peni" class="form-label font-weight-bold">Сумма долги пени</label>
            <input type="text" name="summa_dolgi_peni" class="form-control" value="{{$debtor->summa_dolgi_peni}}">
        </div>

        <div class="mb-3">
            <label for="summa_priznano" class="form-label font-weight-bold">Сумма признано судом</label>
            <input type="text" name="summa_priznano" class="form-control" value="{{$debtor->summa_priznano}}">
        </div>

        <div class="mb-3">
            <label for="summa_priznano_peni" class="form-label font-weight-bold">Признано судом пени</label>
            <input type="text" name="summa_priznano_peni" class="form-control" value="{{$debtor->summa_priznano_peni}}">
        </div>

        <div class="mb-3">
            <label for="summa_vziskano" class="form-label font-weight-bold">Сумма взысканной задолженности</label>
            <input type="text" name="summa_vziskano" class="form-control" value="{{$debtor->summa_vziskano}}">
        </div>

        <div class="mb-3">
            <label for="summa_vziskano_peni" class="form-label font-weight-bold">Сумма взысканной задолженности По пени</label>
            <input type="text" name="summa_vziskano_peni" class="form-control" value="{{$debtor->summa_vziskano_peni}}">
        </div>

        <div class="mb-3">
            <label for="summa_other" class="form-label font-weight-bold">Взыскание других судебных расходов</label>
            <input type="text" name="summa_other" class="form-control" value="{{$debtor->summa_other}}">
        </div>

        <button type="submit" class="btn btn-primary">Обновить</button>
        <button type="submit" class="btn btn-secondary" style="float: right;"><a onclick="javascript:history.back(); return false;">Назад</a></button>
    </form>
</div>


@endsection