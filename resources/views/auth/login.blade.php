@extends('layouts.login-header')

@section('content')

<div class="login-wrapper">
    <img src="/assets/img/login-bg.png" alt="login-bg" class="login-bg">

    <div class="login-container">
        <div class="login">
            <div class="login-left">
                <img src="/assets/img/login-logo.png" alt="login-logo" class="login-left__logo">
                <p class="login-left__subtitle">{{__('lang.header_slogan')}}</p>
            </div>
            <div class="login-right">
                <h2 class="login-right__title"></h2>
                <form method="POST" action="{{ route('login') }}" class="login-form">
                    @csrf
                    <div class="login-form__wrapper">
                        <img src="/assets/img/icons/login-icon.png" alt="login-icon" class="login-form__icon">
                        <input type="email" class="login-form__input" id="email" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('Login') }}" autofocus />
                    </div>
                    <div class="login-form__wrapper">
                        <img src="/assets/img/icons/password-icon.png" alt="password-icon" class="login-form__icon">
                        <input type="password" class="login-form__input" name="password" id="password" autocomplete="current-password" placeholder="{{ __('Password') }}" />
                    </div>
                    <label for="login-checkbox" class="login-form__label_checkbox">
                        <input name="login-checkbox" id="login-checkbox" type="checkbox" class="login-form__checkbox">Remember me
                        <span class="login-form__chekmark"></span>
                    </label>
                    <button class="login-form__btn">Log In</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection