<?php

return [

    'header_slogan' => 'Monitoring the collection process of receivables',

    // header bill
    'all_sum' => 'Total principal amount',
    'all_sum_peni' => 'The total amount of singing',
    'name_debtor' => 'Name of the debtor',
    'calc_amount' => 'Calculation of the amount of the claim',
    'priznannaya_sudom' => 'Amount recognized by the court',
    'amount_debt' => 'Amount of debt collected',
    'calc' => 'Calculated',
    'total' => 'Total',
    'priznano_sudom' => 'Recognized by the court',
    'collected' => 'Collected',
    'in_percentages' => 'In percentages',
    'debt' => 'Debts',
    'peni' => 'Peni',
    'sum' => 'sum',

    // content
    'in_progress' => 'In progress',
    'completed' => 'Completed',
    'debt_recovery' => 'Debt recovery percentage',
    'stage' => 'stage',
    'debtors' => 'Debtors',
    'complete' => 'Completed',

    // nav
    'naimenovaniye_doljnika' => 'Name of the debtor',
    'raschet_summa_iska' => 'Calculation of the claim amount',
    'prizanannaya_summa_sudom' => 'Amount recognized by the court',
    'summa_vziskannoy_zadojennosti' => 'Amount of debt collected',
    'prosent_vziskannoy_zadoljennosti' => 'Percentage of debt recovered',
    'vziskaniye_drugix_sudebnix_rasxodov' => 'Recovery of other court costs',
    'po_osnovnomu_dolgu' => 'On the main debt',
    'po_peni' => 'By peny',


    //stage
    'prosess_naxoditsya_izujeniye' => 'The process is under study',
    'raschot_summi_iska' => 'Calculation of the claim amount',
    'izucheniye' => 'Изучение',
    'do_sudebnogo_resheniye' => 'Before the dispute is resolved',
    'podgotovka_trebovaniya' => 'Preparation of requirements',
    'otpravka_trebovaniya' => 'Submitting a claim',
    'peregovoty_s_doljnikom' => 'Negotiations with the debtor',
    'resheniye_spora_sudebnim' => 'Dispute resolution by litigation',
    'podgotovka_iska' => 'Preparing a claim',
    'otpravka_iska' => 'Sending a claim',
    'rassmotreniye_dela_v_sude' => 'Consideration of the case in court',
    'poluchenie_resheniye' => 'Obtaining a decision and a writ of execution',
    'otpravka_dokumentov' => 'Sending documents to BPI',
    'ispolneniya_resheniya' => 'Execution of a court decision',

    //adminka

    'all_debtors' => 'All debtors'

];
