<?php

return [

    'header_slogan' => 'Мониторинг процесс взыскания дебиторской задолженности',

    // header bill
    'all_sum' => 'Общая сумма основного долга',
    'all_sum_peni' => 'Общая сумма пении',
    'name_debtor' => 'Наименование должника',
    'calc_amount' => 'Расчет суммы иска',
    'priznannaya_sudom' => 'Признанная сумма судом',
    'amount_debt' => 'Сумма взысканной задолженности',
    'calc' => 'Рассчитано',
    'total' => 'Всего',
    'priznano_sudom' => 'Признано судом',
    'collected' => 'Взыскано',
    'in_percentages' => 'В процентах',
    'debt' => 'Долга',
    'peni' => 'Пени',
    'sum' => 'сум',

    // content
    'in_progress' => 'В процессе',
    'completed' => 'Завершено',
    'debt_recovery' => 'Процент взысканной задолженности',
    'stage' => 'этап',
    'debtors' => 'Должники',
    'complete' => 'Завершенные',

    // nav
    'naimenovaniye_doljnika' => 'Наименование должника',
    'raschet_summa_iska' => 'Расчет суммы иска',
    'prizanannaya_summa_sudom' => 'Признанная сумма судом',
    'summa_vziskannoy_zadojennosti' => 'Сумма взысканной задолженности',
    'prosent_vziskannoy_zadoljennosti' => 'Процент взысканной задолженности',
    'vziskaniye_drugix_sudebnix_rasxodov' => 'Взыскание других судебных расходов',
    'po_osnovnomu_dolgu' => 'По основному долгу',
    'po_peni' => 'По пени',

    //stage

    'prosess_naxoditsya_izujeniye' => 'Процесс находиться в стадии изучение',
    'raschot_summi_iska' => 'Расчет суммы иска',
    'izucheniye' => 'Изучение',
    'do_sudebnogo_resheniye' => 'До судебное решение спора',
    'podgotovka_trebovaniya' => 'Подготовка требования',
    'otpravka_trebovaniya' => 'Отправка требования',
    'peregovoty_s_doljnikom' => 'Переговоры с должником',
    'resheniye_spora_sudebnim' => 'Решение спора судебным разбирательством',
    'podgotovka_iska' => 'Подготовка иска',
    'otpravka_iska' => 'Отправка иска',
    'rassmotreniye_dela_v_sude' => 'Рассмотрение дела в суде',
    'poluchenie_resheniye' => 'Получение решение и исполнительного листа',
    'otpravka_dokumentov' => 'Отправка документов в БПИ',
    'ispolneniya_resheniya' => 'Исполнения решения суда',

    // admin

    'all_debtors' => 'Все должники'

];
