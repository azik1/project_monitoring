<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Debtor extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'stages',
        'summa_dolgi',
        'summa_dolgi_peni',
        'summa_priznano',
        'summa_priznano_peni',
        'summa_vziskano',
        'summa_vziskano_peni',
        'summa_other'
    ];
}
