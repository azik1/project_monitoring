<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Debtor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AdminDebtorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Debtor::all();
        return view('backend.debtor.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.debtor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required'
        ]);

        $data = new Debtor([
            'name' => $request->input('name'),
            'slug' => Str::slug($request->input('name')),
            'stages' => $request->input('stages'),
            'summa_dolgi' => $request->input('summa_dolgi'),
            'summa_dolgi_peni' => $request->input('summa_dolgi_peni'),
            'summa_priznano' => $request->input('summa_priznano'),
            'summa_priznano_peni' => $request->input('summa_priznano_peni'),
            'summa_vziskano' => $request->input('summa_vziskano'),
            'summa_vziskano_peni' => $request->input('summa_vziskano_peni'),
            'summa_other' => $request->input('summa_other')
        ]);

        $data->save();
        return redirect('admin/debtor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data = DB::table('debtors')->where('slug', $slug)->first();
        return view('backend.debtor.show', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Debtor $debtor)
    {
        return view('backend.debtor.edit', compact('debtor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Debtor $debtor)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $debtor->update($request->all());

        return redirect('admin/debtor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Debtor $debtor)
    {
        $debtor->delete();

        return redirect()->route('debtor.index');
    }
}
