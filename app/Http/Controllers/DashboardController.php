<?php

namespace App\Http\Controllers;

use App\Models\Dashboard;
use App\Models\Debtor;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $debtor = Debtor::get();

        $stage_first = DB::table('debtors')->whereIn('stages', array(1, 2))->get();
        $stage_second = DB::table('debtors')->whereIn('stages', array(3, 4))->get();
        $stage_three = Debtor::whereIn('stages', array(5, 6, 7, 8, 9, 10, 11, 12))->get();
        $stage_end = Debtor::whereIn('stages', array(13))->get();

        // rasschitano
        $total_sum = Debtor::sum('summa_dolgi');
        $total_peni = Debtor::sum('summa_dolgi_peni');
        $total_sum_peni = $total_sum + $total_peni;

        // priznano sudom
        $total_priznano_sum = Debtor::sum('summa_priznano');
        $total_priznano_peni = Debtor::sum('summa_priznano_peni');
        $total_priznano_sum_peni = $total_priznano_sum + $total_priznano_peni;

        // vziskano
        $total_vzikano = Debtor::sum('summa_vziskano');
        $total_vzikano_peni = Debtor::sum('summa_vziskano_peni');
        $total_vzikano_sum_peni = $total_vzikano + $total_vzikano_peni;

        //prosent
        $total_all_dolg_prosent = $total_vzikano / $total_sum * 100;
        $total_all_peni_prosent = $total_vzikano_peni / $total_peni * 100;
        $total_prosent = $total_all_dolg_prosent + $total_all_peni_prosent;
        $total_all_prosent = $total_prosent / 2;


        return view('frontend.dashboard', [
            'debtors' => $debtor,
            'stage_first' => $stage_first,
            'stage_second' => $stage_second,
            'stage_three' => $stage_three,
            'stage_end' => $stage_end,
            'total_sum' => $total_sum,
            'total_peni' => $total_peni,
            'total_sum_peni' => $total_sum_peni,
            'total_priznano_sum' => $total_priznano_sum,
            'total_priznano_peni' => $total_priznano_peni,
            'total_priznano_sum_peni' => $total_priznano_sum_peni,
            'total_vzikano' => $total_vzikano,
            'total_vzikano_peni' => $total_vzikano_peni,
            'total_vzikano_sum_peni' => $total_vzikano_sum_peni,
            'total_all_dolg_prosent' => round($total_all_dolg_prosent),
            'total_all_peni_prosent' => round($total_all_peni_prosent),
            'total_all_prosent' => round($total_all_prosent)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data = DB::table('debtors')->where('slug', $slug)->first();
        return view('frontend.debtor.show', [
            'data' => $data
        ]);
    }
}
