<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\backend\AdminDebtorController;
use App\Http\Controllers\backend\AdminDashboardController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('dashboard');
});

Auth::routes();

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['auth', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::get('/dashboard', [DashboardController::class, 'index']);
        Route::get('/dashboard/{slug}', [DashboardController::class, 'show']);
    }
);

Route::prefix('/admin')->middleware('auth')->group(function () {
    Route::resource('/debtor', AdminDebtorController::class);
    Route::get('/', [AdminDashboardController::class, 'index']);
    Route::resource('/role', RoleController::class);
    Route::resource('/permission', PermissionController::class);
});
